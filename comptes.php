<?php

namespace bankProject {

    include_once 'agences.php';
    include_once 'clients.php';
    include_once 'utilities.php';

    class Comptes
    {

        private ?int $idComptes = null;
        private ?string $type = null;
        private ?float $solde = null;
        private ?bool $decouvert = null;
        private ?int $idAgences = null;
        private ?int $idClients = null;

        /**
         * __construct
         * @param  mixed $idBanque
         * @param  mixed $type
         * @param  mixed $solde
         * @param  mixed $decouvert
         * @param  mixed $idAgences
         * @param  mixed $idClients
         * @return void
         */
        public function __construct(
            ?int $idComptes = null,
            ?string $type = null,
            ?float $solde = null,
            ?bool $decouvert = null,
            ?int $idAgences = null,
            ?int $idClients = null
        ) {
            $this->idAgences = $idAgences;
            $this->idClients = $idClients;
            $this->idComptes = $idComptes;
            $this->nom = $type;
            $this->adresse = $solde;
            $this->decouvert = $decouvert;
        }

        /**
         * getIdBanque
         *
         * @return int
         */
        public function getIdComptes(): int
        {
            return $this->idBanque;
        }

        /**
         * setIdBanque
         *
         * @param  mixed $idBanque
         * @return void
         */
        public function setIdComptes($idComptes)
        {
            $this->idComptes = $idComptes;
        }

        /**
         * getType
         *
         * @return string
         */
        public function getType(): string
        {
            return $this->type;
        }

        /**
         * setType
         *
         * @param  mixed $type
         * @return void
         */
        public function setType($type)
        {
            $this->type = $type;
        }

        /**
         * getSolde
         *
         * @return float
         */
        public function getSolde(): float
        {
            return $this->solde;
        }

        /**
         * setSolde
         *
         * @param  mixed $solde
         * @return void
         */
        public function setSolde($solde)
        {
            $this->solde = $solde;
        }

        /**
         * getDecouvert
         *
         * @return bool
         */
        public function getDecouvert(): bool
        {
            return $this->decouvert;
        }

        /**
         * setDecouvert
         *
         * @param  mixed $decouvert
         * @return void
         */
        public function setDecouvert($decouvert)
        {
            $this->decouvert = $decouvert;
        }

        /**
         * getIdAgences
         *
         * @return int
         */
        public function getIdAgences(): int
        {
            return $this->idAgences;
        }

        /**
         * getIdClients
         *
         * @return int
         */
        public function getIdClients(): int
        {
            return $this->idClients;
        }

        public function createAccount($cntComptes): array
        {

            if (isset($clients) && isset($agences)) {

                $this->idClients = readline("Saisir l'identifiant du client à associer : ");
                $this->idAgences = readline("Saisir le code le l'agence à associer : ");
                $this->type = readline("Choisir le type de compte (Livret A, Compte courant, Plan épargne logement) : ");

                if ($cntComptes < 10) {
                    $this->idComptes = "000000000" . $cntComptes;
                } elseif ($cntComptes > 9 && $cntComptes < 100) {
                    $this->idComptes = "00000000" . $cntComptes;
                } elseif ($cntComptes > 99 && $cntComptes < 1000) {
                    $this->idComptes = "000000" . $cntComptes;
                } elseif ($cntComptes > 999 && $cntComptes < 10000) {
                    $this->idComptes = "00000" . $cntComptes;
                } elseif ($cntComptes > 99999 && $cntComptes < 100000) {
                    $this->idComptes = "0000" . $cntComptes;
                } elseif ($cntComptes > 999999 && $cntComptes < 1000000) {
                    $this->idComptes = "000" . $cntComptes;
                } elseif ($cntComptes > 9999999 && $cntComptes < 10000000) {
                    $this->idComptes = "00" . $cntComptes;
                } elseif ($cntComptes > 99999999 && $cntComptes < 100000000) {
                    $this->idComptes = "0" . $cntComptes;
                } elseif ($cntComptes > 999999999 && $cntComptes < 1000000000) {
                    $this->idComptes = $cntComptes;
                }

                $this->solde = randomSolde();

                if ($this->solde[0] === "-") {
                    $this->decouvert = "O";
                } else {
                    $x = rand(0, 1);
                }
                if ($x == 1) {
                    $this->decouvert = "N";
                } else {
                    $this->decouvert = "O";
                }

            } else {

                $this->idClients = readline("Saisir l'identifiant du client à associer : ");
                $this->idAgences = readline("Saisir le code le l'agence à associer : ");
                $this->type = readline("Choisir le type de compte (Livret A, Compte courant, Plan épargne logement) : ");

                if ($cntComptes < 10) {
                    $this->idComptes = "000000000" . $cntComptes;
                } elseif ($cntComptes > 9 && $cntComptes < 100) {
                    $this->idComptes = "00000000" . $cntComptes;
                } elseif ($cntComptes > 99 && $cntComptes < 1000) {
                    $this->idComptes = "000000" . $cntComptes;
                } elseif ($cntComptes > 999 && $cntComptes < 10000) {
                    $this->idComptes = "00000" . $cntComptes;
                } elseif ($cntComptes > 99999 && $cntComptes < 100000) {
                    $this->idComptes = "0000" . $cntComptes;
                } elseif ($cntComptes > 999999 && $cntComptes < 1000000) {
                    $this->idComptes = "000" . $cntComptes;
                } elseif ($cntComptes > 9999999 && $cntComptes < 10000000) {
                    $this->idComptes = "00" . $cntComptes;
                } elseif ($cntComptes > 99999999 && $cntComptes < 100000000) {
                    $this->idComptes = "0" . $cntComptes;
                } elseif ($cntComptes > 999999999 && $cntComptes < 1000000000) {
                    $this->idComptes = $cntComptes;
                }

                //   --------- SOLDE ---------

                $this->solde = randomSolde();

                if ($this->solde[0] === "-") {
                    $this->decouvert = "O";
                } else {
                    $x = rand(0, 1);
                }
                if ($x == 1) {
                    $this->decouvert = "N";
                } else {
                    $this->decouvert = "O";
                }
            }
        
        }
        /**
         * __toString
         *
         * @return string
         */
        public function __toString(): string
        {
            return "Compte n° $this->idComptes   du client   $this->idClient crée";
        }
    }
}
