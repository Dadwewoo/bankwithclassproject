<?php

namespace bankProject {

    class Agences
    {
        private ?array $agences = [];
        private ?int $idAgences = null;
        private ?string $nom = null;
        private ?string $adresse = null;

        /**
         * __construct
         * @param  mixed $idAgences
         * @param  mixed $nom
         * @param  mixed $adresse
         * 
         * @return void
         */
        public function __construct(
            ?array $agences = [],
            ?int $idAgences = null,
            ?string $nom = null,
            ?string $adresse = null
        ) {
            $this->agences = $agences;
            $this->idAgences = $idAgences;
            $this->nom = $nom;
            $this->adresse = $adresse;
        }

        /**
         * getIdAgences
         *
         * @return int
         */
        public function getIdAgences(): int
        {
            return $this->idAgences;
        }

        /**
         * setIdAgences
         *
         * @param  mixed $idAgences
         * @return void
         */
        public function setIdAgences($idAgences)
        {
            $this->id = $idAgences;
        }

        /**
         * getNom
         *
         * @return string
         */
        public function getNom(): string
        {
            return $this->nom;
        }

        /**
         * setNom
         *
         * @param  mixed $nom
         * @return void
         */
        public function setNom($nom)
        {
            $this->nom = $nom;
        }

        /**
         * getAdresse
         *
         * @return string
         */
        public function getAdresse(): string
        {
            return $this->adresse;
        }

        /**
         * setAdresse
         *
         * @param  mixed $adresse
         * @return void
         */
        public function setAdresse($adresse)
        {
            $this->adresse = $adresse;
        }

        /**
         * createAgency
         *
         * @return void
         */
        public function createAgency()
        {
            $this->agences[$this->nom] = readline("Veuillez entrer votre nom d'agence : ");
            $this->agences[$this->adresse] = readline("Veuillez entrer l'adresse de votre agence : ");
            if (isset($cntAgence) < 10) {
                $this->agences[$this->idAgences] = "00" . isset($cntAgence);
            } elseif (isset($cntAgence) > 9 && isset($cntAgence) < 100) {
                $this->agences[$this->idAgences] = "0" . isset($cntAgence);
            } else {
                $this->agences[$this->idAgences] = isset($cntAgence);
            }
            return $this->agences;
        }

        /**
         * __toString
         *
         * @return string
         */
        public function __toString(): string
        {
            return "votre agence $this->nom  à pour code agence le numero $this->idAgences .\n";
        }
    }
}
