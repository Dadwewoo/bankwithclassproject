<?php

namespace bankProject {

    include_once 'utilities.php';

    class Clients
    {
        private ?array $clients = [];
        private ?int $idClients = null;
        private ?string $nom = null;
        private ?string $prenom = null;

        /**
         * __construct
         * @param  mixed $idClients
         * @param  mixed $nom
         * @param  mixed $prenom
         * 
         * @return void
         */
        public function __construct(
            ?array $clients = [],
            ?int $idClients = null,
            ?string $nom = null,
            ?string $prenom = null
        ) {
            $this->clients = $clients;
            $this->idClients = $idClients;
            $this->nom = $nom;
            $this->prenom = $prenom;
        }

        /**
         * getIdClients
         *
         * @return int
         */
        public function getIdClients(): int
        {
            return $this->idClients;
        }

        /**
         * setIdClients
         *
         * @param  mixed $idClients
         * @return void
         */
        public function setIdClients($idClients)
        {
            $this->idClients = $idClients;
        }

        /**
         * getNom
         *
         * @return string
         */
        public function getNom(): string
        {
            return $this->nom;
        }

        /**
         * setNom
         *
         * @param  mixed $nom
         * @return void
         */
        public function setNom($nom)
        {
            $this->nom = $nom;
        }

        /**
         * getPrenom
         *
         * @return string
         */
        public function getPrenom(): string
        {
            return $this->prenom;
        }

        /**
         * setPrenom
         *
         * @param  mixed $prenom
         * @return void
         */
        public function setPrenom($prenom)
        {
            $this->prenom = $prenom;
        }

        /**
         * creer_client
         *
         * @param  mixed $cntClient
         * @return array
         */
        function createCustomer($cntClient): array
        {
            if ($cntClient < 10) {
                $this->clients[$this->idClients] = randomString() . randomString() . "00000" . $cntClient;
            } elseif ($cntClient > 9 && $cntClient < 100) {
                $this->clients[$this->idClients] = "0000" . $cntClient;
            } elseif ($cntClient > 99 && $cntClient < 1000) {
                $this->clients[$this->idClients] = "000" . $cntClient;
            } elseif ($cntClient > 999 && $cntClient < 10000) {
                $this->clients[$this->idClients] = "00" . $cntClient;
            } elseif ($cntClient > 9999 && $cntClient < 100000) {
                $this->clients[$this->idClients] = "0" . $cntClient;
            } else {
                $this->clients[$this->idClients] = $cntClient;
            }

            $this->clients[$this->prenom] = readline("Saisir le prénom du client : ");
            $this->clients[$this->nom] = readline("Saisir le nom du client : ");

            return $this->clients;
        }

        /**
         * __toString
         *
         * @return string
         */
        public function __toString(): string
        {
            return "Client : $this->prenom $this->nom  ID : $this->idClients  crée.";
        }
    }
}
